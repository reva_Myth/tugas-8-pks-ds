@extends('layout.master')
@section('title')
Halaman List cast
@endsection
@section('content')

<a href="/cast/create" class="btn btn-primary mb-4">Tambah Cast</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">nama</th>
        <th scope="col">umur</th>
        <th scope="col">bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key=>$item)
          <tr>
            
            <td>{{$key+1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>{{$item->bio}}</td>
            <td>
                <a href="/cast/{{$item->id}}"class="btn btn-info btn-sm">Detail</a>
                <a href="/cast/{{$item->id}}/edit"class="btn btn-warning btn-sm">Edit</a>
                <form action="/cast/{{$item->id}}" method="POST">
                @csrf
                @method('delete')
                <input type="submit" value="delete" class="btn btn-danger btn-sm">
            </form>
            </td>

          </tr>
      @empty
          <h1>Data Tidak Ada</h1>
      @endforelse
    </tbody>
  </table>
@endsection
