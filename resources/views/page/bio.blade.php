@extends('layout.master')
@section('title')
Halaman Form
@endsection
@section('content')
    <h1>Buat Account Baru</h1>
    <h4>Sign Up Form</h4>
        <form action="/welcome" method="POST">
            @csrf
            <label>First Name :</label><br><br>
                <input type="text" name="fname"><br><br>
            <label>Last Name  :</label><br><br>
                <input type="text" name="lname"><br><br>
            <label>Gender</label><br><br>
                <input type="radio" name="gender"> Male <br>
                <input type="radio" name="gender"> Female <br><br>
            <label>Nationality</label><br><br>
                <select name="Nation" id="">
                    <option value="1">Indonesia</option>
                    <option value="2">Amerika</option>
                    <option value="3">Inggris</option>
                </select><br><br>
            <label>Language Spoken</label><br><br>
                <input type="checkbox"name="Language"> Bahasa Indonesia <br>
                <input type="checkbox"name="Language"> English <br>
                <input type="checkbox"name="Language"> Other <br><br>
            <label>Bio</label><br><br>
                <textarea name="Bio"  cols="30" rows="10"></textarea><br>
                <input type="Submit" value="Sign Up">
            </form>
@endsection