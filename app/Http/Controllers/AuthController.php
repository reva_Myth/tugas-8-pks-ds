<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function reg()
    {
        return view('page.bio');
    }
    public function welcome(Request $request)
    {
        //dd($request->all());
        $fname=$request['fname'];
        $lname=$request['lname'];

        return view('page.welcome',compact("fname","lname"));
    }
}
