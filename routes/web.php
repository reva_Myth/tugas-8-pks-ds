<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@reg');
Route::post('/welcome','AuthController@welcome');
Route::get('/data-table','IndexController@table');

//CRUD Cast
//create
Route::get('/cast/create','CastController@create');//menampilkan form untuk membuat data pemain film baru
Route::post('/cast','CastController@store');//menyimpan data baru ke tabel Cast

//readdata
Route::get('/cast','CastController@index');//ambil datakedatabase
Route::get('/cast/{cast_id}','CastController@show'); //Route detail kategori
//update
Route::get('/cast/{cast_id}/edit','CastController@edit');
Route::put('/cast/{cast_id}','CastController@update');
//delete
Route::delete('/cast/{cast_id}','CastController@destroy');
